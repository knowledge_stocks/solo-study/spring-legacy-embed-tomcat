package com.mycompany.SpringTest;

import org.apache.catalina.core.StandardContext;
import org.apache.catalina.loader.WebappLoader;
import org.apache.catalina.startup.Tomcat;

import java.io.File;

public class EmbedTomcatMain {
  public static void main(String[] args) throws Exception {
    System.out.println("EmbedTomcatMain Started");

    // 인자로 넘어온 포트번호를 가져옴
    if(args.length == 0) {
      throw new Exception("첫번째 인자로 포트번호를 입력해주세요.");
    }
    String portStr
        = args[0];
//        = "8080";
    int port;
    try {
      port = Integer.parseInt(portStr);
    } catch (NumberFormatException e) {
      throw new Exception("포트번호는 숫자 형식으로 입력해주세요");
    }

    // 시스템 속성으로 설정된 war 파일의 이름을 가져옴
    String warFilePath
        = System.getProperty("WAR_FILE_PATH");
//        = "target/SpringTest-1.0.0-BUILD-SNAPSHOT.war";
    File warFile = new File(warFilePath);
    if(!warFile.exists())
    {
      throw new Exception("시스템 속성값 WAR_FILE_PATH로 war파일의 경로를 지정해주세요.");
    }

    File baseDir = new File("target/tomcat/");
    System.out.println("Set Embedded Tomcat's Base directory to '" + baseDir.getAbsolutePath() + "'");
    Tomcat tomcat = new Tomcat();
    tomcat.setBaseDir(baseDir.getAbsolutePath());

    File appBase = new File(baseDir, "webapps");
    System.out.println("Set Embedded Tomcat's appBase to '" + appBase.getAbsolutePath() + "'");
    if(!appBase.exists()) {
      appBase.mkdirs();
    }
    tomcat.getHost().setAppBase(appBase.getAbsolutePath());

    System.out.println("Set port number to " + port);
    tomcat.setPort(port);

    // ROOT context에 war파일을 등록해준다.
    System.out.println("Add webapp context '/', " + warFile.getAbsolutePath());
    StandardContext ctx = (StandardContext) tomcat.addWebapp("/", warFile.getAbsolutePath());
    WebappLoader loader = new WebappLoader(Thread.currentThread().getContextClassLoader());
    ctx.setLoader(loader);

    tomcat.start();
    tomcat.getServer().await();
  }
}
